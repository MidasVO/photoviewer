"use strict";
(function () {
var bodyNode,
    mainDiv,
    mainNode,
    grid,
    headerDiv,
    divName,
    images,
    counter,
    iLength,
    imageContainer,
    imgSrc,
    mode = "thumb",
    info,
    photographer,
    title,
    name,
    photographerText,
    titleText,
    nameText,
    amountImages,
    divider,
    currentImage = 0;

var Photoviewer = {


/**
 * Creates the main div
 * @return {[type]} [description]
 */
  createMain: function () {
    bodyNode = document.getElementsByTagName("body")[0];
    mainDiv = document.createElement("div");
    mainDiv.id = "main";
    bodyNode.appendChild(mainDiv);
  },
/**
 * Creates the grid div and adds the id and classes
 * @param  {[type]} divide [description]
 * @return {[type]}        [description]
 */
  createGrid: function () {
  if(amountImages === undefined) {
    amountImages = 25;
  }
  if (amountImages > 20 && amountImages <= 25) {
      divider = 5;
  } else if (amountImages > 15 && amountImages <= 20) {
      divider = 4;
  } else if (amountImages > 10 && amountImages <= 15) {
      divider = 3;
  } else if (amountImages > 5 && amountImages <= 10) {
      divider = 2;
  } else if (amountImages <= 5) {
      divider = 1;
  }
    mainNode = document.getElementById("main");
    grid = document.createElement("div");
    grid.id = "grid";
    grid.className = "grid";
    grid.className += " grid--divide-" + divider;
    mainNode.appendChild(grid);

  },
  /**
   * Creates the image div and adds the class and onclick functions
   * @param  {[type]} id [description]
   * @return {[type]}    [description]
   */
  createImage: function (id) {
    if(mode === "zoom") {
      imageContainer = document.createElement("div");
      imageContainer.id = "zoom";
      mainDiv.appendChild(imageContainer);
      pv.createImageSrc(id);
    } else {
    imageContainer = document.createElement("div");
    imageContainer.className = "grid_unit";
    imageContainer.addEventListener('click', function(){
        pv.zoom(id);
        currentImage = id;
    });

    // context menu
    imageContainer.oncontextmenu = function () {
      this.remove(); //remove current image
      amountImages--;
      // TODO: adjust the grid accordingly
      return false;
    };
      imageContainer.id = img.data[id].id;
      grid.appendChild(imageContainer);
      pv.createImageSrc(id);
    }
  },
  /**
   * Creates the image source element
   * @param  {[type]} id [description]
   * @return {[type]}    [description]
   */
  createImageSrc: function (id) {
    imgSrc = document.createElement("img");
    imgSrc.src = "./images/" + img.data[id].name;
    imageContainer.appendChild(imgSrc);
  },
  // handle the spacebar events
  handleSpacebar: function () {
    if(mode === "zoom") {
      // already zoomed in, so go back to the grid
      mainDiv.remove();
      pv.createMain();
      pv.createGrid();
      mode = "thumb";

      pv.putImages(mode);
    } else if (mode === "thumb") { 
      // we're at the grid view, so zoom in on img.data[0]
      pv.zoom(0);
      mode = "zoom";

    }
  },
  // zoom in on one picture
  zoom: function (id) {
      mode = "zoom";
      console.log(id);
      pv.removeGrid();


      info = document.createElement("div");
      info.id = "info";
      mainDiv.appendChild(info);

      // title, photographer, name
      title = document.createElement("h4");
      photographer = document.createElement("p");
      name = document.createElement("p");

      titleText = document.createTextNode("Title: " + img.data[id].title);
      photographerText = document.createTextNode("Photographer: " + img.data[id].photographer);
      nameText = document.createTextNode("Filename: " + img.data[id].name);

      title.appendChild(titleText);
      photographer.appendChild(photographerText);
      name.appendChild(nameText);

      info.appendChild(title);
      info.appendChild(photographer);
      info.appendChild(name);

      pv.createImage(id);
  },

  /**
   * [removeGrid description]
   * @return {[type]} [description]
   */
  removeGrid: function () {
    grid.remove();
  },
  /**
   * [removeMain description]
   * @return {[type]} [description]
   */
  removeMain: function () {
    mainDiv.remove();
  },
  removeZoom: function () {
    zoom.remove();
    info.remove();
  },
  /**
   * Generate main and grid
   * @return {[type]} [description]
   */
  regenerate: function () {
    pv.removeZoom();
    pv.createMain();
    pv.createGrid();
    pv.putImages();
  },
  /**
   * Call to create all images in JSON array
   * @return {[type]} [description]
   */
  putImages: function (mode) {
    images = img.data;
    iLength = images.length;
    counter = 0;
    amountImages = iLength;
    console.log(iLength);
    for(counter = 0; counter < iLength; counter++) {
      pv.createImage(counter); // counter passes ID
      if(mode === "thumb") {
        imageContainer.style.addClass = "grid_unit";
      }
    }
  },
  nextImage: function() {
    if(mode === "thumb") {
      // do nothing
    } else if(mode === "zoom") {
      currentImage++;
     if(iLength === currentImage) {
      currentImage = 0;
     }
      pv.removeZoom();
      pv.zoom(currentImage);
    }
  },
  previousImage: function() {
    if(mode === "thumb") {
      // do nothing
    } else if(mode === "zoom") {
     if(currentImage <= 0) {
      currentImage = 25;
     }
     currentImage--;
      pv.removeZoom();
      pv.zoom(currentImage);
    }
  }



};

/**
 * Image object
 * @type {Object}
 */
var Images = {
  "data":[
      {"id": "0","title":"Palmboom","photographer":"Hans Aarsman","name":"0004681.jpg"},
      {"id": "1","title":"Fiets","photographer":"Emmy Andriesse","name":"0004713.jpg"},
      {"id": "2","title":"Sahara","photographer":"Iwan Baan ","name":"0004720.jpg"},
      {"id": "3","title":"Chinees","photographer":"Henze Boekhout","name":"0004731.jpg"},
      {"id": "4","title":"Bouwvakker","photographer":"Anton Corbijn","name":"0004750.jpg"},
      {"id": "5","title":"Gezin","photographer":"Rineke Dijkstra","name":"0004755.jpg"},
      {"id": "6","title":"Pruim","photographer":"Ed van der Elsken","name":"0004801.jpg"},
      {"id": "7","title":"Keanu Reeves","photographer":"Carli Hermès","name":"0004802.jpg"},
      {"id": "8","title":"Swingers","photographer":"Rob Hornstra","name":"0004827.jpg"},
      {"id": "9","title":"Brug","photographer":"Ad Konings ","name":"0004853.jpg"},
      {"id":"10","title":"Staart","photographer":"Jeroen Kramer","name":"0004858.jpg"},
      {"id":"11","title":"Pier","photographer":"Inez van Lamsweerde","name":"0004860.jpg"},
      {"id":"12","title":"Duuude","photographer":"Frans Lanting","name":"0004870.jpg"},
      {"id":"13","title":"Gezin2","photographer":"Erwin Olaf","name":"0004874.jpg"},
      {"id":"14","title":"SARS","photographer":"Levi van Veluw","name":"0004888.jpg"},
      {"id":"15","title":"Swingers2","photographer":"Laurence Aberhart","name":"0004902.jpg"},
      {"id":"16","title":"Brug2","photographer":"Brian Brake","name":"0004931.jpg"},
      {"id":"17","title":"teste","photographer":"Anne Geddes","name":"0004969.jpg"},
      {"id":"18","title":"Golf","photographer":"Geoff Moon","name":"0004971.jpg"},
      {"id":"19","title":"Vet gebouw","photographer":"Herzekiah Andrew Shanu","name":"0006598.jpg"},
      {"id":"20","title":"Nog een leuke titel","photographer":"Rolf Aamot","name":"0006630.jpg"},
      {"id":"21","title":"NEC > Vitesse","photographer":"Catherine Cameron","name":"0006638.jpg"},
      {"id":"22","title":"teste","photographer":"Frode Fjerdingstad","name":"0006680.jpg"},
      {"id":"23","title":"teste","photographer":"Petter Hegre","name":"0006743.jpg"},
      {"id":"24","title":"teste","photographer":"Luca Kleve-Ruud ","name":"0006787.jpg"}
  ]
};






    window.pv = Photoviewer;
    window.img = Images;

}());

window.onload=function () {
    pv.createMain();
    pv.createGrid();
    pv.putImages();
};
window.addEventListener('keyup',this.handleEvents,false);

function handleEvents(e) {
    var code = e.keyCode;
    switch(code) {
        case 32: 
          pv.handleSpacebar();
        break;

        case 39: // right arrow
              pv.nextImage();
        break;

        case 37: // arrow left
              pv.previousImage();
        break;


    }

}
