# Photoviewer#

HTML5, CSS3 and JavaScript photogallery. Right click to delete from the DOM and left click to enter zoom mode on the image. Zoom mode can also be enabled/disabled by using the space bar. 

### Author ###

* Midas van Oene